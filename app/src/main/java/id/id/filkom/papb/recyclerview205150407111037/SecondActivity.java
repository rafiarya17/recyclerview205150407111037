package id.id.filkom.papb.recyclerview205150407111037;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {
    TextView teviewNama;
    TextView teviewNIM;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        teviewNama = findViewById(R.id.teviewNama);
        teviewNIM = findViewById(R.id.teviewNIM);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        Intent intent = getIntent();
        String name = intent.getStringExtra("Nama");
        String nim = intent.getStringExtra("Nim");

        teviewNama.setText("Nama : " +name);
        teviewNIM.setText("NIM : "+nim);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnBack) {
            finish();
        }
    }
}