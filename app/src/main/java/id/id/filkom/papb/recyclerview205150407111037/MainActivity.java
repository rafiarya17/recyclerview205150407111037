package id.id.filkom.papb.recyclerview205150407111037;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static String TAG = "recyclerView";
    EditText etNama;
    EditText etNIM;
    Button btn1;
    RecyclerView review1;

    ArrayList<Mahasiswa> daftarMahasiswa;
    MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        review1 = findViewById(R.id.review1);
        etNama = findViewById(R.id.etNama);
        etNIM = findViewById(R.id.etNIM);
        btn1 = findViewById(R.id.btn1);
        daftarMahasiswa = new ArrayList<>();
        adapter = new MahasiswaAdapter(this, daftarMahasiswa);
        review1.setAdapter(adapter);
        review1.setLayoutManager(new LinearLayoutManager(this));
        btn1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.nama = etNama.getText().toString();
        mahasiswa.nim = etNIM.getText().toString();

        daftarMahasiswa.add(mahasiswa);
        adapter.notifyDataSetChanged();
    }
}